#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "Credentials.h"

WiFiClient espClient;
PubSubClient mqtt(espClient);
const char *mqttTopic = "esp8266/meter/1";

const int AOUT1 = 4;
const int AOUT10 = 5;
const int AOUT100 = 16;
unsigned long lerpMillis = 0;
const unsigned long lerpInterval = 100;
float lerpDelta = 0;
uint32_t travelMillis = 0;
const uint32_t travelInterval = 1000;
unsigned long connectCheckMillis = 0;
const unsigned long connectCheckInterval = 10000;

const int METERS = 3; // Index 0 is units
int digitVal[METERS];
int digitOld[METERS];
const int pin[METERS] = { 4, 5, 16 };
int debugVal = 444;

const int pwm[] = { 
  160, // 0 y
  240, // 1 y
  310, // 2 y
  380, // 3 y
  450, // 4 y
  525, // 5 y
  600, // 6 y
  675, // 7 y
  750, // 8 y
  825};// 9 y

void messageFromMQTT(char* topic, byte* payload, unsigned int payloadLength);

void setup() {
  // GPIO
  for (int meter = 0; meter < METERS; meter++) {
    pinMode(pin[meter], OUTPUT);
    analogWrite(pin[meter], 0);
    digitVal[meter] = 0; 
  }
  // Serial
  Serial.begin(115200);
  while (!Serial);
  // WiFi
  WiFi.mode(WIFI_STA);
  WiFi.begin(Credentials::SSIDName, Credentials::SSIDPassword);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println(WiFi.localIP());
  // MQTT
  mqtt.setServer(Credentials::MQTTIPAddress, Credentials::MQTTIPPort);
  mqtt.setCallback(messageFromMQTT);
}

void messageFromMQTT(char* topic, byte* payload, unsigned int payloadLength) {
  Serial.println(topic);
  //unsigned long currentMillis = millis();
  //messageInterval = currentMillis - messageMillis;
  //messageMillis = currentMillis;

  DynamicJsonBuffer jsonBuffer(payloadLength);
  JsonObject &root = jsonBuffer.parseObject(payload);
  float fvalue = root["value"];
  int16_t ivalue = (int16_t) fvalue;
  for (int meter = 0; meter < METERS; meter++) {
    int digit = (ivalue / (int) pow(10,meter)) % 10;
    digitOld[meter] = digitVal[meter];
    digitVal[meter] = pwm[digit];
    Serial.print(digit);
    lerpMillis = travelMillis = millis();
    lerpDelta = 0;
  }
  Serial.println();
}

void connectMQTT() {
  if (mqtt.connect("ESP8266Meter", Credentials::MQTTUsername, Credentials::MQTTPassword)) {
    if ( mqtt.subscribe(mqttTopic) ) {
      Serial.print("subscribed ");
      Serial.println(mqttTopic);
    } else {
      Serial.println("subscribe failed");
    }
  } else {
    Serial.print("connect failed ");
    Serial.println(mqtt.state());
  }  
}

void loop() {
  
  unsigned long currentMillis = millis();
  if (currentMillis - lerpMillis >= lerpInterval) {
    lerpMillis = currentMillis;
    if (currentMillis - travelMillis > travelInterval) {
      for (int meter = 0; meter < METERS; meter++) {
        analogWrite(pin[meter], digitVal[meter]); 
      }
    } else {
      float ratio = lerpDelta / travelInterval;
  
      for (int meter = 0; meter < METERS; meter++) {
        int posDelta = ratio * (digitVal[meter] - digitOld[meter]);
        analogWrite(pin[meter], digitOld[meter] + posDelta); 
      }
      lerpDelta += lerpInterval;
    }
  }
  
  if ( ! mqtt.connected() ) {
    connectMQTT();
  }
  mqtt.loop();
}


