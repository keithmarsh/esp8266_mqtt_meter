# ESP8266_MQTT_Meter

A simple MQTT data sink that listens on "meter" for a three digit value and displays it on three analogue meters.

Because meters are rarely linear (cheap ones anyway), there's a lookup table for meter values for each digit.  Tune this for your meters.
